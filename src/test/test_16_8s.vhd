library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.ALL;
use std.textio.ALL;

entity test_16_8s is
end test_16_8s;

architecture behavior of test_16_8s is
	constant N : positive := 100;

	component DDD16_8s is
	port (
		CLK : in std_logic;
		dividend : in unsigned(15 downto 0);
		divisor : in unsigned ( 7 downto 0);
		quotient, reminder : out unsigned (7 downto 0) := (others => '0')
	);
	end component;
	
    signal clk : std_logic := '0';
    constant clkperiod : time := 1 us;
	signal count : integer := N;
	signal cnt : unsigned(7 downto 0);
begin

test: DDD16_8s
	port map (
		CLK => clk,
		dividend => unsigned(to_signed(-1000, 16)),
		divisor => cnt,
		quotient => open,
		reminder => open
	);

	clk <= not clk after clkperiod / 2 when count /= -N else unaffected;

	count <= count - 1 when falling_edge(clk);

	cnt <= to_unsigned(count, 8);
	
end behavior;
