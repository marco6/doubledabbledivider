library IEEE;

use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;

entity DDDu is
	port (
		CLK 	 : in std_logic;
		dividend : in unsigned(15 downto 0);
		divisor  : in unsigned ( 7 downto 0);
		quotient, reminder : out unsigned (7 downto 0) := (others => '0')
	);
end DDDu;

architecture behavioral of DDDu is
begin
	process (CLK)
		variable res : unsigned(15 downto 0);
		variable add : unsigned(7 downto 0);
	begin
		if(rising_edge(CLK)) then
			res := (others => '0');
			add := (0-divisor);
			for i in dividend'range loop
				res := res(14 downto 0) & dividend(i);
				if(res(7 downto 0) >= divisor) then
					res := res + add;
				end if;
			end loop;
			quotient <= res(15 downto 8);
			reminder <= res(7 downto 0);
		end if;
	end process;
end behavioral;
