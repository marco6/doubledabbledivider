library IEEE;

use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;

entity DDD16_8s is
	port (
		CLK : in std_logic;
		dividend : in unsigned(15 downto 0);
		divisor : in unsigned ( 7 downto 0);
		quotient, reminder : out unsigned (7 downto 0) := (others => '0')
	);
end DDD16_8s;

architecture behavioral of DDD16_8s is
	component DDD16_8u is
	port (
		CLK : in std_logic;
		dividend : in unsigned(15 downto 0);
		divisor : in unsigned ( 7 downto 0);
		quotient, reminder : out unsigned (7 downto 0) := (others => '0')
	);
	end component;
	signal dd : unsigned(15 downto 0);
	signal dv, qt : unsigned ( 7 downto 0);
	signal neg : std_logic;
begin
	
u_div: DDD16_8u
	port map(CLK => CLK,
		dividend => dd,
		divisor => dv,
		quotient => qt,
		reminder => reminder
	);

	neg <= divisor(divisor'left) xor dividend(dividend'left);
	
	dd <= unsigned(abs(signed(dividend)));
	dv <= unsigned(abs(signed(divisor)));
	quotient <= unsigned(-signed(qt)) when neg='1' else qt;

end behavioral;
